# Blender Community FR

Logo Creation files for the logo of the French speaking community on [blender.community](https://blender.community/c/)

The project .blend file must be opened with Blender 2.8 or higher.

This work is licensed under a [**Creative Commons Attribution-ShareAlike 4.0 International License**](https://creativecommons.org/licenses/by-sa/4.0/).

------------------------

Available resolutions:

| 64px                                                         | 256px                                                        | 512px                                                        |
| ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ |
| [![64px](Blender_community_Fr_64px.png)](Blender_community_Fr_64px.png) | [![256px](Blender_community_Fr_256px.png)](Blender_community_Fr_256px.png) | [![512px](Blender_community_Fr_512px.png)](Blender_community_Fr_512px.png) |

